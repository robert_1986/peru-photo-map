(function(document) {

    var app = document.getElementById('my-app');

    app.getIndex = function (i) {
        return i+1;
    };

    app.markerUrl = function (i) {
        return "http://mapmarkers.rcurran.co.uk/marker/blue_pin/000000/" + i;
    };

    app.thumbUrl = function(img) {
        return "https://s3.amazonaws.com/peru-photo-map/thumbs/" + img;
    };

    app.fullSizeUrl = function(img) {
        return "https://s3.amazonaws.com/peru-photo-map/full/" + img;
    };

    app.getDialogId = function(id) {
        return "dialog-" + id;
    };

    openFull = function(e) {
        var dialog = document.getElementById('card-img-dialog');
        dialog.innerHTML = "";
        dialog.appendChild(loader);
        dialog.open();

        var fullImg = e.target.src.replace("/thumbs/", "/full/");
        var image = new Image();
        image.onload = function() {
            dialog.innerHTML = "";
            dialog.appendChild(image);
            dialog.center();
        }
        image.src = fullImg;
    };

    document.addEventListener('WebComponentsReady', function () {
        // Set default tab to open when the page loads
        var template = document.querySelector('template[is="dom-bind"]');
        template.selected = 0; // selected is an index by default
    });

    var loader = new Image();
    loader.src = "styles/loader.gif";

})(document);
